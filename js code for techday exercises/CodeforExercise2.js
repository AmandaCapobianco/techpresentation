/**
* This is Component 2 - You will see how to build your own component and use props.
*                       Using name as a prop to customize the Welcome component
*                       This also demonstrates how to incorporate basic styles
*/

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View} from 'react-native';

class Welcome extends Component {
  render(){
    return(
      <Text style={styles.bigred}>Welcome {this.props.name}!</Text>
    );
  }
}

export default class MultipleWelcomes extends Component {
  render() {
    return(
      <View style={{alignItems: 'center'}}>
        <Welcome name='John' />
        <Welcome name='Ashley' />
        <Welcome name='Sam' />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bigred: {
    color: 'red',
    fontSize: 20,
  },
});


AppRegistry.registerComponent('MultipleWelcomes', () => MultipleWelcomes);
