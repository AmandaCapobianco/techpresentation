/**
* This is Component 3 - You will practice how to use a List View
*/
import React, {Component} from 'react';
import {AppRegistry, Platform,Text, View, ListView} from 'react-native';

  const students = [
    {name: 'Sarah Smith'},
    {name: 'William Jones'},
    {name: 'Rachel Doe'},
    {name: 'Tom Lopez'},

  ]

export default class Component3 extends Component{

    constructor(){
      super();
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
          userDataSource: ds.cloneWithRows(students),
      };
    }

    renderRow(student, sectionId, rowId, highlightRow){
      return(
      <View>
          <Text>{student.name}</Text>
      </View>
    )


    }

  render(){
    return(
      <ListView
        dataSource={this.state.userDataSource}
        renderRow={this.renderRow.bind(this)}
      />
    );
  }
}

AppRegistry.registerComponent('Component3', () => Component3);
