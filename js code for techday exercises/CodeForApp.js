/**
 * This is my PresentationApp - How to get started with React Native
 * Amanda Capobianco
*/

import React, {Component} from 'react';
import {AppRegistry, Platform, StyleSheet, Text, View} from 'react-native';


export default class PresentationApp extends Component{
  render(){
    return(
      <View>
        <Text>Welcome CUS1166 Class!</Text>
        </View>
    );
  }
}

AppRegistry.registerComponent('PresentationApp', () => PresentationApp);
